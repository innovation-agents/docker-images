# Innovation Agents Docker Images

### hetzner-gitlab-runner
* Eigenes Image passend zum Artikel https://www.marc-willmann.de/gitlab-runner-autoscale-in-der-hetzner-cloud. 

### ia-pipeline
* Kleines Image auf Basis von `docker:latest`, ausgestattet mit allen notwendigen Tools, um den Pipelines nicht ständig das Image wechseln zu müssen
* Enthaltene Pakete: php, nodejs, yarn, composer, gettext, npm, curl
