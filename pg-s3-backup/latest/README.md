# pg-s3-backup

> Docker Image with Alpine Linux, pg_dump and awscli for backup all databases of a single postgres server to s3

## Use

### Periodic backup

Run every day at 2 am

```bash
docker run -d --name pgdump \
  -e "DB_HOST=host" \
  -e "DB_PORT=5432" \
  -e "DB_USER=username" \
  -e "DB_PASSWORD=***" \
  -e "S3_BUCKET=your_aws_bucket" \
  -e "AWS_ACCESS_KEY_ID=your_aws_access_key" \
  -e "AWS_SECRET_ACCESS_KEY=your_aws_secret_access_key" \
  -e "AWS_REGION=us-west-1" \
  -e "BACKUP_CRON_SCHEDULE=0 2 * * *" \
  registry.gitlab.com/innovation-agents/docker-images/pg-s3-backup:latest
```

### Inmediatic backup

```bash
docker run -d --name pgdump \
  -e "DB_HOST=host" \
  -e "DB_PORT=5432" \
  -e "DB_USER=username" \
  -e "DB_PASSWORD=***" \
  -e "S3_BUCKET=your_aws_bucket" \
  -e "AWS_ACCESS_KEY_ID=your_aws_access_key" \
  -e "AWS_SECRET_ACCESS_KEY=your_aws_secret_access_key" \
  -e "AWS_REGION=us-west-1" \
  registry.gitlab.com/innovation-agents/docker-images/pg-s3-backup:latest
```

## Extra environment

- `MAX_BACKUPS` - Default not set. If set doing it keeps the last n backups in /backup
- `S3_ENDPOINT` - Default not set. Override default AWS S3 Endpoint
- `S3_PATH` - Default not set. Set bucket path prefix for backups
