#!/usr/bin/env php
<?php

function writeln (string|array $text, ...$variables): void
{
    write($text, ...$variables);
    echo PHP_EOL;
}

function write (string|array $text, ...$variables): void
{
    if (is_array($text)) {
        $text = implode(PHP_EOL, $text);
    }

    echo sprintf($text, ...$variables);
}

function execute (string $command, ...$variables): mixed
{
    $cmd = sprintf($command, ...$variables);
    exec($cmd, $result, $resultCode);

    if ($resultCode === 0) {
        return $result;
    }

    writeln($result);
    exit($resultCode);
}

function executeAwsS3 (?string $endpoint, string $command, ...$variables): mixed
{
    array_unshift($variables, $endpoint);

    $command = null === $endpoint
        ? 'aws s3 '.$command
        : 'aws --endpoint-url=%s s3 '.$command
    ;

    return execute($command, ...$variables);
}

function checkVariables (): void
{
    $variables = ['DB_HOST', 'DB_PORT', 'DB_USER', 'DB_PASSWORD', 'S3_BUCKET', 'AWS_REGION', 'AWS_ACCESS_KEY_ID', 'AWS_SECRET_ACCESS_KEY'];

    foreach ($variables as $variable) {
        if (empty($_SERVER[$variable])) {
            writeln('the following env variables must be set: '.implode(', ', $variables));
            exit(255);
        }
    }
}

function createDirectory (string $dir): void
{
    if (!is_dir($dir)) {
        $created = mkdir($dir, 0777, true);
        if (!$created) {
            writeln('Cannot create Backup Directory '.$dir);
            exit(255);
        }
    }
}

function fetchDatabases (string $host, int $port, string $user, string $password): array
{
    $result = execute('PGPASSWORD=%s psql -h %s -p %d --username %s --list -P pager=off -P format=csv', $password, $host, $port, $user);

    $dbs = [];

    foreach ($result as $index => $line) {
        if ($index < 1 || !str_contains($line, ',')) {
            continue;
        }

        $db = substr($line, 0, strpos($line, ','));
        if (empty($db) || str_starts_with($db, 'template')) {
            continue;
        }

        $dbs[] = $db;
    }

    if (empty($dbs)) {
        writeln('no databases found');
        exit(255);
    }

    return $dbs;
}

function backupDatabase (string $host, int $port, string $username, string $password, string $backupDir, string $database, string $s3Endpoint, string $s3Bucket, string $s3Path = null): string
{
    $backupDir .= '/'.$database;
    $backupFileName = sprintf('%s__%s.sql', $database, date('Y-m-d__H-i-s'));
    $backupFilePath = $backupDir.'/'.$backupFileName;

    createDirectory($backupDir);

    $compressedBackupFileName = $backupFileName .'.gz';
    $compressedBackupFilePath = $backupDir.'/'.$compressedBackupFileName;

    $databasePath = null === $s3Path
        ? $database
        : rtrim($s3Path, '/').'/'.$database
    ;

    write('%s to %s/%s ... ', $database, $databasePath, $backupFileName);

    // create uncompressed database dump
    execute(
            'PGPASSWORD=%s pg_dump --clean --if-exists --no-acl --no-owner -h %s -p %s -U %s -f %s %s',
        $password,
        $host,
        $port,
        $username,
        $backupFilePath,
        $database
    );

    // compress dump
    execute('cd %s && tar -czf "%s" %s', $backupDir, $compressedBackupFileName, $backupFileName);

    // transfer to s3
    executeAwsS3($s3Endpoint, 'cp "%s" "s3://%s/%s/%s"', $compressedBackupFilePath, $s3Bucket, $databasePath, $compressedBackupFileName);

    // clean up
    unlink($backupFilePath);
    unlink($compressedBackupFilePath);

    writeln('OK');

    return $backupDir.'/'.$compressedBackupFileName;
}

function cleanOutdatedFiles (string $s3Endpoint, string $s3Bucket, ?string $s3Path = null, int $maxFiles = 10): void
{
    $path = null === $s3Path
        ? $s3Bucket
        : $s3Bucket.'/'.ltrim($s3Path, '/')
    ;

    $files = executeAwsS3($s3Endpoint, 'ls s3://%s --recursive', $path);

    $list = [];

    foreach ($files as $filePath) {

        $filePath = trim(substr($filePath, 19));
        $filePath = trim(substr($filePath, strpos($filePath, ' ')));

        $file = $filePath;

        // remove path prefix
        if ($s3Path) {
            $file = str_replace(trim($s3Path, '/'), '', $filePath);
            $file = trim($file, '/');
        }

        $database = substr($file, 0, strpos($file, '/'));

        if (!isset($list[$database])) {
            $list[$database] = [];
        }

        $list[$database][] = $filePath;
    }

    foreach ($list as $backups) {
        if (count($backups) < $maxFiles) {
            continue;
        }

        sort($backups, SORT_NATURAL);
        $reversedFiles = array_reverse($backups);

        foreach ($reversedFiles as $index => $file) {
            if ($index < $maxFiles) {
                continue;
            }

            write($file.' ... ');

            executeAwsS3($s3Endpoint, 'rm s3://%s/%s', $s3Bucket, $file);

            writeln('OK');
        }
    }
}

checkVariables();

$host = $_SERVER['DB_HOST'];
$port = $_SERVER['DB_PORT'];
$user = $_SERVER['DB_USER'];
$password = $_SERVER['DB_PASSWORD'];
$s3Bucket = $_SERVER['S3_BUCKET'];
$s3Endpoint = $_SERVER['S3_ENDPOINT'] ?? null;
$s3Path = $_SERVER['S3_PATH'] ?? null;
$maxBackups = $_SERVER['MAX_BACKUPS'] ?? 10;

$backupDir = '/tmp/backup';

createDirectory($backupDir);

// backup all databases

writeln('BACKUP ...');
$databases = fetchDatabases($host, $port, $user, $password);
foreach ($databases as $database) {
    backupDatabase($host, $port, $user, $password, $backupDir, $database, $s3Endpoint, $s3Bucket, $s3Path);
}

writeln('');

// delete outdated files

writeln('CLEANUP (Max Backups per database: %d) ...', $maxBackups);
cleanOutdatedFiles($s3Endpoint, $s3Bucket, $s3Path, $maxBackups);

writeln('');

// done

writeln('DONE');
