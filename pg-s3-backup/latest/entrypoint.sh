#!/usr/bin/env sh

set +e

if [ -z ${BACKUP_CRON_SCHEDULE+x} ]; then
  php81 /usr/local/bin/backup.php
else
  BACKUP_CRON_SCHEDULE=${BACKUP_CRON_SCHEDULE}
  echo "${BACKUP_CRON_SCHEDULE} php81 /usr/local/bin/backup.php" > /etc/crontabs/root
  # Starting cron
  crond -f -d 0
fi

exec "$@"
