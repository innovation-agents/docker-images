for folder in *; do
  if [ -d "$folder" ]; then
    cd $folder || exit 1
    for tag in *; do
      if [ -d "$tag" ]; then
        docker build --file $tag/Dockerfile --tag $CI_REGISTRY_IMAGE/$folder:$tag $folder
        docker push $CI_REGISTRY_IMAGE/$tag
      fi
    done
    cd ..
  fi
done
